﻿using SOPLister.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SOPLister.Controllers
{
    public class HomeController : Controller
    {
        private String rootPath
        {
            get
            {
                return WebConfigurationManager.AppSettings["rootPath"];
            }
        }

        private int replaceIndex
        {
            get
            {
                return Convert.ToInt32(WebConfigurationManager.AppSettings["replaceIndex"]);
                
            }
        }
        private String replaceContent
        {
            get
            {
                return WebConfigurationManager.AppSettings["replaceContent"];
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult SOPS()
        {
            //String rootDir = "\\\\hona02\\dfs$\\SOP's";
            DirectoryEnumerator enumerator = new DirectoryEnumerator(rootPath, replaceIndex, replaceContent);
            return View(enumerator.contents);
        }
    }
}