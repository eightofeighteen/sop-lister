﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace SOPLister.Models
{
    public class DirectoryContents
    {
        public String DirectoryName { get; set; }
        public List<String> Files;
        public List<DirectoryContents> Directories;

        private String rootPath;
        private int replaceIndex;
        private String replaceContent;

        public DirectoryContents(String rootPath, int replaceIndex, String replaceContent)
        {
            Files = new List<String>();
            Directories = new List<DirectoryContents>();
            this.rootPath = rootPath;
            this.replaceIndex = replaceIndex;
            this.replaceContent = replaceContent;
        }

        public override string ToString()
        {
            return RToString(this);
        }

        public string ToHTML()
        {
            return "<table>\n" + RToHTML(this) + "</table>\n";
        }

        private String RToHTML(DirectoryContents contents)
        {
            String s = "<tr><td class=\"directory\">" + contents.DirectoryName + "</td></tr>\n";
            if (contents.Files.Count == 0)
                s = s + "<tr><td class=\"file\">(no files)</td></tr>\n";
            foreach (String file in contents.Files)
            {
                s = s + "<tr><td class=\"file\"><a href=\"" + translate(file) + "\">🖺 " + getFileName(file) + "</a></td></tr>\n";
            }
            foreach (var directory in contents.Directories)
            {
                s = s + RToHTML(directory) + "\n";
            }


            return s;
        }

        private String RToString(DirectoryContents contents)
        {
            String s = contents.DirectoryName + "\n";
            if (contents.Files.Count == 0)
                s = s + "\t(no files)\n";
            foreach (String file in contents.Files)
            {
                s = s + "\t" + file + "\n";
            }
            foreach (var directory in contents.Directories)
            {
                s = s + RToString(directory) + "\n";
            }


            return s;
        }

        static private String getFileName(String fullName)
        {
            int pos = fullName.LastIndexOf('\\');
            return fullName.Substring(pos + 1);
        }

        private String translate(String fullName)
        {
            //return fullName;
            //int pos = fullName.IndexOf('\\', fullName.IndexOf("\\\\"));
            //return fullName.Substring(pos + 1);
            //int pos = 14;
            //String replace = "";
            return replaceContent + fullName.Substring(replaceIndex);
        }
    }

    public class DirectoryEnumerator
    {
        private String rootDir;
        public DirectoryContents contents;

        private int replaceIndex;
        private String replaceContent;

        public DirectoryEnumerator(String rootDir, int replaceIndex, String replaceContent)
        {
            this.rootDir = rootDir;
            this.replaceIndex = replaceIndex;
            this.replaceContent = replaceContent;
            contents = new DirectoryContents(rootDir, replaceIndex, replaceContent);
            process();
        }

        private void process()
        {
            contents.DirectoryName = rootDir;
            contents = processElement(contents);
        }

        DirectoryContents processElement(DirectoryContents elem)
        {
            DirectoryContents contents = new DirectoryContents(rootDir, replaceIndex, replaceContent);

            contents.DirectoryName = elem.DirectoryName;
            //Console.WriteLine(contents.DirectoryName);
            contents.Files = Directory.GetFiles(contents.DirectoryName).ToList();
            foreach (var dir in Directory.GetDirectories(contents.DirectoryName))
            {
                DirectoryContents leaf = new DirectoryContents(rootDir, replaceIndex, replaceContent);
                leaf.DirectoryName = dir;
                contents.Directories.Add(processElement(leaf));
            }

            return contents;
        }

        public override String ToString()
        {
            return contents.ToString();
        }
    }
}